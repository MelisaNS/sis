<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengamatan extends Model
{
    use HasFactory;
    protected $table= 'pengamatan';
    protected $fillable = [
        'kelembapan_udara',
        'kelembapan_tanah',
        'suhu_udara',
        'id_perangkat',
        'id_penyiraman'
    ];

    public function data_penyiraman()
    {
        return $this->belongsTo('App\Models\Data_penyiraman', 'id_pengamatan', 'id_penyiraman');
    }
}
