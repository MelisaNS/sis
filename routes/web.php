<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'App\Http\Controllers\AuthController@login')->name('login');
Route::post('/postlogin', 'App\Http\Controllers\AuthController@postlogin');
Route::get('/register', 'App\Http\Controllers\AuthController@register');
Route::post('/postregister', 'App\Http\Controllers\AuthController@postregister');
Route::get('/logout', 'App\Http\Controllers\AuthController@logout');


Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index');
    Route::get('/chart', 'App\Http\Controllers\ChartController@index'); //ini routenya
    Route::get('/smart_garden', 'App\Http\Controllers\SmartController@index');
    Route::delete('/smart_garden/{id_perangkat}', 'App\Http\Controllers\SmartController@delete');
});
