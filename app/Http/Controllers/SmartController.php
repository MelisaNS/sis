<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pengamatan;

class SmartController extends Controller
{
    public function index()
    {
        $data_pengamatan = Pengamatan::all();
        return view('smart_garden.index', compact('data_pengamatan'));
    }

    public function delete($id_pengamatan)
    {
        DB::table('pengamatan')->where('id_pengamatan', $id_pengamatan)->delete();
        return redirect('/smart_garden')->with('sukses', 'Data berhasil dihapus');
    }
}
