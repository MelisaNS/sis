@extends('layouts.Master')

@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                    <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">DATA PENNGAMATAN</h3>
                                    <div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up lnr-chevron-down"></i></button>
									</div>
								</div>
                                
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
                                                <th> No </th>
                                                <th> Kelembapan Udara </th>
                                                <th> Kelembapan Tanah </th>
                                                <th> Suhu Udara </th>
                                                <th> Tanggal Penyiraman </th>
                                                <th> Waktu Penyiraman </th>
                                                <th>Aksi</th>
											</tr>
										</thead>
										<tbody>
                                        @foreach($data_pengamatan as $pengamatan)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{$pengamatan->kelembapan_udara}}</td>
                                            <td>{{$pengamatan->kelembapan_tanah}}</td>
                                            <td>{{$pengamatan->suhu_udara}}</td>
                                            <td>{{$pengamatan->data_penyiraman->tgl_penyiraman}}</td>
                                            <td>{{$pengamatan->data_penyiraman->waktu_penyiraman}}</td>
                                                            
                                            <td>
                                            <form action="{{ url('smart_garden/' .$pengamatan->id_pengamatan) }}" method="POST" class="d-inline" onsubmit="return confirm('Yakin mau dihapus?')">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-danger btn-sm"> <span class="lnr lnr-trash"></span> </button>
                                            </form>
                                            </td>
                                        </tr>
                                        @endforeach
										</tbody>
									</table>
								</div>
							</div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop 



