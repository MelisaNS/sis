@extends('layouts.Master')

@section('content')
<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<!-- OVERVIEW -->
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">Dashboard</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-drop"></i></span>
								<p>
									<span class="number">{{ $pengamatan->suhu_udara }}</span>
									<span class="title">Suhu Udara</span>
								</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-drop"></i></span>
								<p>
									<span class="number">{{ $pengamatan->kelembapan_udara }}</span>
									<span class="title">Kelembapan Udara</span>
								</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-drop"></i></span>
								<p>
									<span class="number">{{ $pengamatan->kelembapan_tanah }}</span>
									<span class="title">Kelembapan Tanah</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel">
				<div id="chartPengamatan">

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<style>
	.highcharts-data-table table {
		min-width: 310px;
		max-width: 800px;
		margin: 1em auto;
	}

	#chartPengamatan {
		height: 400px;
	}

	.highcharts-data-table table {
		font-family: Verdana, sans-serif;
		border-collapse: collapse;
		border: 1px solid #EBEBEB;
		margin: 10px auto;
		text-align: center;
		width: 100%;
		max-width: 500px;
	}

	.highcharts-data-table caption {
		padding: 1em 0;
		font-size: 1.2em;
		color: #555;
	}

	.highcharts-data-table th {
		font-weight: 600;
		padding: 0.5em;
	}

	.highcharts-data-table td,
	.highcharts-data-table th,
	.highcharts-data-table caption {
		padding: 0.5em;
	}

	.highcharts-data-table thead tr,
	.highcharts-data-table tr:nth-child(even) {
		background: #f8f8f8;
	}

	.highcharts-data-table tr:hover {
		background: #f1f7ff;
	}
</style>

<script>
	// Create the chart

	Highcharts.chart('chartPengamatan', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Grafik Smart Garden'
		},
		xAxis: {
			categories: [
				'Sen',
				'Sel',
				'Rab',
				'Kam',
				'Jum',
				'Sab',
				'Ming'
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Pengamatan'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.1f}%</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [{
			name: 'Pengamatan',
			data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, ]

		}]
	});
</script>

@stop