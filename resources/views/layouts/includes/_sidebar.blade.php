<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				<li><a href="/dashboard" class="{{ request()->is('dashboard')? 'active' : ''}}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
				<li><a href="/smart_garden" class="{{ request()->is('smart_garden')? 'active' : ''}}"><i class="lnr lnr-chart-bars"></i><span>Pengamatan</span></a></li>
			</ul>
		</nav>
	</div>
</div>