<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pengamatan;

class DashboardController extends Controller
{
    public function index()
    {
        $data = [
            'pengamatan' => Pengamatan::orderBy('id_pengamatan', 'desc')->first()
        ];
        return view('dashboards.index', $data);
    }
}
