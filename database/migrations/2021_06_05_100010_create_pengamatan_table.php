<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengamatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengamatan', function (Blueprint $table) {
            $table->bigincrements('id_pengamatan');
            $table->string('kelembapan_udara',20);
            $table->string('kelembapan_tanah',20);
            $table->string('suhu_udara',20);

            $table->biginteger('id_perangkat')->unsigned();
            $table->foreign('id_perangkat')->references('id_perangkat')->on('perangkat')->onDelete('cascade');

            $table->biginteger('id_penyiraman')->unsigned();
            $table->foreign('id_penyiraman')->references('id_penyiraman')->on('data_penyiraman')->onDelete('cascade');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengamatan');
    }
}
