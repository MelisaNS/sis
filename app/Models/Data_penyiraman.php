<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data_penyiraman extends Model
{
    use HasFactory;
    protected $table= 'data_penyiraman';

    protected $fillable = [
        'tgl_penyiraman',
        'waktu_penyiraman'
    ];
}
