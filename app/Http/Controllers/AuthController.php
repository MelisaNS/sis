<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('auths.login');
    }

    public function postlogin(Request $request)
    {
        if(Auth::attempt($request->only('email','password'))){
            return redirect('/dashboard');
        }

        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function register()
    {
        return view('auths.register');
    }

    public function postregister(Request $request)
    {
        //input pendaftaran sebagai users
        $user = new \App\Models\User;
        $user->role = 'register';
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt ($request->password);
        $user->remember_token = str_random(60);
        $user->save();

        //input table register
        $request->request->add(['id'=>$user->id]);
        $register = \App\Models\Register::create($request->all());

        return redirect('/register')->with('sukses','Berhasil Register');
         
    }
}
